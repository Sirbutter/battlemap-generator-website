package main

import (
	"context"
	"go-micro.dev/v4"
	"log"

	first "battlemap-generator/proto/firstpack"
)

type Greeter struct{}

func (g *Greeter) Hello(ctx context.Context, req *first.HelloRequest, rsp *first.HelloResponse) error {
	rsp.Greeting = "Hello " + req.Name
	return nil
}

func main() {
	service := micro.NewService(
		micro.Name("greeter"),
	)

	service.Init()

	err := first.RegisterGreeterHandler(service.Server(), new(Greeter))
	if err != nil {
		return
	}

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
